# Badge

Documentation and examples for badges, our small count and labeling component.

Read the original [Bootstrap 5 documentation](https://getbootstrap.com/docs/5.0/components/badge) for more information.
