# Alert

Provide contextual feedback messages for typical user actions with the handful of available and flexible alert messages.

If you want to take full control of the Alert content, use the `{% block content %}` slot. For example, to get the icon layout [as appears on the official documentation](https://getbootstrap.com/docs/5.0/components/alerts/#icons), you can do the following:

```twig
{% embed 'radix:alert' with {
  header: label,
  attributes: attributes.addClass(['d-flex', 'align-items-center'])
} only %}
  {% block content %}
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-exclamation-triangle-fill       flex-shrink-0 me-2" viewBox="0 0 16 16" role="img" aria-label="Warning:">
      <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
    </svg>
    <div>
      An example alert with an icon
    </div>
  {% content %}
{% endembed %}
```

Note that, in order to get the correct classes on the component wrapper, we need to pass the `attributes` object updated with our custom classes.

Read the original [Bootstrap 5 documentation](https://getbootstrap.com/docs/5.0/components/alerts/) for more information.
