# Blockquote

For quoting blocks of content from another source within your document.

Read the original [Bootstrap 5 documentation](https://getbootstrap.com/docs/5.2/content/typography/#blockquotes) for more information.

## Where is the CSS?

Blockquotes on Bootstrap 5 form part of a broader file dedicated to
typographic components: `bootstrap/scss/type`. This is why this component don't
feature it's own SCSS / CSS file and is instead loaded globally.

Any overrides for this component should take control of the entire `_type.scss`
file (or use SCSS variables).
