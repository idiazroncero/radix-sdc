# Radix
[![Documentation Status](https://readthedocs.org/projects/radix/badge/?version=latest)](http://docs.radixtheme.org/en/latest/?badge=latest)

Radix is a base theme for Drupal. It has Bootstrap 4, Sass, BrowserSync and Font
Awesome built-in.

![alt tag](https://www.drupal.org/files/radix-steps.png)

# Installation

radix_starterkit theme uses [Webpack](https://webpack.js.org) to compile and
bundle SASS and JS.

#### Step 1
Make sure you have Node and npm installed.
You can read a guide on how to install node here:
https://docs.npmjs.com/getting-started/installing-node

If you prefer to use [Yarn](https://yarnpkg.com) instead of npm, install Yarn by
following the guide [here](https://yarnpkg.com/docs/install).

#### Step 2
Go to the root of radix_starterkit theme and run the following commands:
`nvm use`

This command triggers the node version switch specified in the `.nvmrc` file
Read more about [NVM](https://github.com/nvm-sh/nvm)

then:
`npm install` or `yarn install`

#### Step 3
Update `proxy` in **config/proxy.js**.

#### Step 4
Run the following command to compile Sass and watch for changes: `npm run watch`
or `yarn watch`.

## Distributions using Radix

##### [Apigee Developer Portal](https://www.drupal.org/project/apigee_devportal_kickstart)
Apigee Developer Kickstart is a distribution to create an Apigee developer
portal using Drupal.

##### [Open Atrium](http://openatrium.com)
Open Atrium is an intranet in a box that has group spaces to allow different
teams to have their own conversations and collaboration.

##### [Open Academy](http://drupal.org/project/openacademy)
Open Academy is a Drupal distribution that brings the best in web publishing for
higher education on a customizable Drupal platform.

##### [Open Berkeley](http://open.berkeley.edu)
Open Berkeley is a turnkey web platform solution for UC Berkeley campus
websites.

##### [Open Restaurant](http://drupal.org/project/openrestaurant)
The Open Restaurant distribution has everything you need to kickstart your
restaurant website. It comes with a menu management system, a reservation
system, a customizable blog and events management.

##### [Sector](https://sector.nz)
Sector is a New Zealand Drupal Distribution aimed at getting your web project
up and running in no time.

##### [DKAN Open Data Platform](https://getdkan.org)
DKAN is a community-driven, free and open source open data platform that gives
organizations and individuals ultimate freedom to publish and consume structured
information.

##### [University of IOWA SiteNow](https://sitenow.uiowa.edu)
SiteNow provides flexibility in authoring websites for various audiences.

## Links
* Project Page:   http://www.radixtheme.org
* Documentation:  http://docs.radixtheme.org
* Support:        https://www.drupal.org/project/issues/radix

## License
http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
